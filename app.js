// Dependencies
const express = require("express")
const mongoose = require("mongoose")
const cors = require ("cors")

// Routes
const userRoutes = require("./routes/userRoutes")
const productRoutes = require("./routes/productRoutes")

// Server
const app = express()
const PORT = 4000

app.use(cors())

app.use(express.json())
app.use(express.urlencoded({extended:true}))

mongoose.connect("mongodb+srv://lriparip:Parexcellence.1@wdc028-course-booking.aaei5.mongodb.net/b170-ecommerce?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"))
db.once("open", () => console.log("We're connected to the database"))

app.use("/api/users", userRoutes)
app.use("/api/products", productRoutes)

app.listen(process.env.PORT || PORT, () => console.log(`API now online at port ${process.env.PORT || PORT}`))