const express = require("express")
const router = express.Router()
const auth = require("../auth.js")
const userController = require("../controllers/userControllers")

// user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result))
})

// user login
router.post("/login", (req, res) => {
	userController.userLogin(req.body).then(result => res.send(result))
})

// set isAdmin to true
router.put("/:userId/setAsAdmin", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization)
	userController.setAsAdmin(req.params, req.body, userData).then(result => res.send(result))
})

// create order
router.post("/checkout", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization)
	userController.createOrder(req.body, userData).then(result => res.send(result))
})

// retrieve all order
router.get("/orders", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization)
	userController.retrieveAllOrder(userData).then(result => res.send(result))
})

// retrieve user orders
router.get("/myOrders", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	userController.myOrders(userData).then(result => res.send(result))
})

router.post("/checkEmail", (req, res) => {
	userController.checkEmail(req.body).then(result => res.send(result))
})

router.get("/details", auth.verify, (req, res)=> {
	const userData = auth.decode(req.headers.authorization)
	userController.getProfile( {userId: userData.id} ).then(resultFromController => res.send(resultFromController))
} )

module.exports = router 