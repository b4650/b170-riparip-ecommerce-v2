const express = require("express")
const router = express.Router()
const auth = require("../auth.js")
const productController = require("../controllers/productControllers")

// Add product
router.post("/addProduct", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	productController.addProduct(req.body, userData).then(result => res.send(result))
})

// Retrieve all active products
router.get("/", (req, res) => {
	productController.getActiveProducts().then(result => res.send(result))
})

// Retrieve a single product
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params.productId).then(result => res.send(result))
})

// Update a product
router.put("/:productId", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	productController.updateProduct(req.params, req.body, userData).then(result => res.send(result))
})

// Archive product
router.put("/:productId/archive", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	productController.archiveProduct(req.params, req.body, userData).then(result => res.send(result))
})

module.exports = router