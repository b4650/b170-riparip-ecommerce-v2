const mongoose = require("mongoose")

const orderSchema = new mongoose.Schema({
		productId:{
			type: String,
			required: [true, "ProductId is required"]
	},
		quantity:{
			type: Number,
			required: [true, "You need to buy at least 1"],
			min: [0, "Sorry, you cannot purchase 0 or less GPUs. You cannot watch Netflix in your PC"]
	},
		price:{
			type: Number
	},
		purschasedOn:{
		type: Date,
		default: new Date()
	},
		userId: {
			type: String,
			required: [true, "User ID is required"]
	}
})

module.exports = mongoose.model("Order", orderSchema) 