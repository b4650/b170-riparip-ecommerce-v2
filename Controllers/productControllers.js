const Order = require("../models/Order")
const Product = require("../models/Product")
const User = require("../models/User")
const auth = require("../auth.js")
const bcrypt = require("bcrypt")

//Add new product
module.exports.addProduct = (reqBody, userData) => {
	return User.findById(userData.userId).then(result => {
		if (userData.isAdmin === false){
			return "You are not an admin"
		}else {
			let newProduct = new Product({
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price
			})
			return newProduct.save().then((product, error) => {
				if(error){
					return false
				}else {
					return "Product successfully added"
				}
			})
		}
	})
}

//Retrieve all active Products
module.exports.getActiveProducts = () => {
	return Product.find({isActive: true}).then((result,error) => {
		if(error) {
			console.log(error)
		}else {
			return result
		}
	})
}

//Retrieve single product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams).then(result => {
		return result
	})
}

//Update a product
module.exports.updateProduct = (reqParams, reqBody, userData) => {
	return User.findById(userData.userId).then(result => {
		if (userData.isAdmin === false){
			return "You are not an admin"
		}else {
			let updatedProduct = {
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price
			}
			return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((result,error) => {
				if(error) {
					console.log(error)
					return false
				}else {
					return true
				}
			})
		}
	})
} 

//Archive a product
module.exports.archiveProduct=(reqParams, reqBody, userData) => {
	return User.findById(userData.userId).then(result => {
		if (userData.isAdmin === false){
			return "You are not an admin"
		}else {
			let updatedProduct = {
				isActive: reqBody.isActive
			}
			return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((result, error) => {
				if(error){
					return false
				}else {
					return true
				}
			})
		}
	})
}