const Order = require("../models/Order")
const Product = require("../models/Product")
const User = require("../models/User")
const auth = require("../auth.js")
const bcrypt = require("bcrypt")

// User Registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		fullName: reqBody.fullName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((saved, error) => {
		if(error){
			return false
		}else {
			return newUser
		}
	})
}

// User Login
module.exports.userLogin = (reqBody) => {
	return User.findOne({email:reqBody.email}).then(result => {
		if (result === null){
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if (isPasswordCorrect){
				return {access: auth.createAccessToken(result.toObject())}
			}else{
				return false
			}
		}
	})
}

// Update to admin
module.exports.setAsAdmin = (reqParams, reqBody, userData) => {
	if (userData.isAdmin === false){
		return "You are not an Admin"
	}else {
		let updatedAdminStatus = {
			isAdmin: reqBody.isAdmin
		}
		return User.findByIdAndUpdate(reqParams.userId, updatedAdminStatus).then ((result, error) => {
			if (error) {
				return false
			}else {
				return true
			}
		})
	}
}

// Create order
module.exports.createOrder = (reqBody, userData, productData) => {
	if(userData.isAdmin === true){
		return "Admin is not allowed to create an order"
	}else {
		let newOrder = new Order ({
			productId: reqBody.productId,
			quantity: reqBody.quantity,
			totalAmount: reqBody.totalAmount,
			userId: reqBody.userId
		})
		return newOrder.save().then((order, error) => {
			if(error) {
				return false
			}else {
				return "Order successfully placed"
			}
		})
	}
}

// Retrieve all orders
module.exports.retrieveAllOrder = (userData) => {
	if(userData.isAdmin === false){
		return "Please contact an admin"
	}else {
		Order.find({}).then((result, error) => {
			if (error) {
				return false
			} else {
				return result
			}
		})
	}
}

// Check email
module.exports.checkEmail = (requestBody => {
	return User.find({email: requestBody.email}).then((result,error) => {
		if (error) {
			console.log(error)
			return false
		}else {
			if (result.length > 0) {
				// return result
				return true
			}else {
				// return res.send("email does not exist")
				return false
			}
		}
	})
})

// Check details
module.exports.getProfile = (data) => {
	return User.findById( data.userId ).then(result =>{
		if (result === null) {
			return false
		} else {
			result.password = "";
			return result
		}
	})
}